package ph.com.sunlife.wms.resource.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/test")
public class TestController {

	@GetMapping("/printMessage")
	public ResponseEntity<Void> printMessage() {
		System.out.println("Hello fucking world!");
		return new ResponseEntity<Void>(HttpStatus.OK);
	}

}