package ph.com.sunlife.wms.resource.util;

public class Constants {

	public static final String AUTH_SERVER = "http://127.0.0.1:8080/auth-server/oauth/token";
	public static final String GRANT_CLIENTCREDENTIALS = "/grant-type/client-credentials/{token}";
	
}
