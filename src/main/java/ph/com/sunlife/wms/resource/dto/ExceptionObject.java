package ph.com.sunlife.wms.resource.dto;

import org.springframework.http.HttpStatus;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter @Getter
@ToString
public class ExceptionObject {

	private int code;
	private String message;
	private String developerMessage;
	private String uri;
	
	public ExceptionObject(HttpStatus httpStatus, String message, String developerMessage, String uri) {
		super();
		this.code = httpStatus.value();
		this.message = message == null ? "System is busy. Please contact your Administrator." : message;
		this.developerMessage = developerMessage;
		this.uri = uri;
	}
	
}