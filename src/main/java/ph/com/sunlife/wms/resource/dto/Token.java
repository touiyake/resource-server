package ph.com.sunlife.wms.resource.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter @Getter
@ToString
public class Token {

	private String access_token;
	private String token_type;
	private long expires_in;
	
	public Token() {
		super();
	}
	
	public Token(String access_token, String token_type, long expires_in) {
		super();
		this.access_token = access_token;
		this.token_type = token_type;
		this.expires_in = expires_in;
	}
	
}
